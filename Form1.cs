﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Net;

namespace WinFormsWeatherEndava
{
    public partial class formWeather : Form
    {
        public formWeather()
        {
            InitializeComponent();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }
 string city;
        private void button1_Click(object sender, EventArgs e)
        {
           
            city = txtcity.Text;
            string uri = string.Format("http://api.weatherapi.com/v1/forecast.xml?key=d1d0a7a187814fc286d74700212107&q=Bucuresti&days=1&aqi=no&alerts=no", city);
            XDocument doc = XDocument.Load(uri);
            string iconUri = (string)doc.Descendants("icon").FirstOrDefault();
            WebClient client = new WebClient();
            byte[] image = client.DownloadData("http:" + iconUri);
            MemoryStream stream = new MemoryStream(image);
            Bitmap newBitMap = new Bitmap(stream);
            string maxtemp = (string)doc.Descendants("maxtemp_c").FirstOrDefault();

            string mintemp = (string)doc.Descendants("mintemp_c").FirstOrDefault();
            string maxwindm = (string)doc.Descendants("maxwind_mph").FirstOrDefault();
            string maxwindk = (string)doc.Descendants("maxwind_kph").FirstOrDefault();
            string humidity = (string)doc.Descendants("avghumidity").FirstOrDefault();
            string country = (string)doc.Descendants("country").FirstOrDefault();
            string cloud = (string)doc.Descendants("text").FirstOrDefault();
            Bitmap icon = newBitMap;
            txtmaxtemp.Text = maxtemp;
            txtmintemp.Text = mintemp;
            txtwindm.Text = maxwindm;
            txtwindk.Text = maxwindk;
            txthumidity.Text = humidity;
            label7.Text = cloud;
            txtcountry.Text = country;
            pictureBox1.Image = icon;


        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void txtcountry_TextChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {



        }
    



        private void button2_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Country", typeof(string));
            dt.Columns.Add("Date", typeof(string));
            dt.Columns.Add("Max Temp", typeof(string));
            dt.Columns.Add("Min Temp", typeof(string));
            dt.Columns.Add("Maxwindmph", typeof(string));
            dt.Columns.Add("Maxwindkph", typeof(string));
            dt.Columns.Add("Humidity", typeof(string));
            dt.Columns.Add("Cloud", typeof(string));
            dt.Columns.Add("Icon", typeof(string));
            city = txtcity.Text;
            string uri = string.Format("http://api.weatherapi.com/v1/forecast.xml?key=d1d0a7a187814fc286d74700212107&q={0}&days=3&aqi=no&alerts=no", city);
            XDocument doc = XDocument.Load(uri);
            foreach (XElement npc in doc.Descendants("forecastday")) 
            {
                string iconUri = (string)npc.Descendants("icon").FirstOrDefault();
                WebClient client = new WebClient();
                byte[] image = client.DownloadData("http:" + iconUri);
                MemoryStream stream = new MemoryStream(image);
                Bitmap newBitMap = new Bitmap(stream);
                dt.Rows.Add(new object[]
           {
               (string)doc.Descendants("country").FirstOrDefault(),
                (string)npc.Descendants("date").FirstOrDefault(),
                (string)npc.Descendants("maxtemp_c").FirstOrDefault(),
                (string)npc.Descendants("mintemp_c").FirstOrDefault(),
                (string)npc.Descendants("maxwind_mph").FirstOrDefault(),
                (string)npc.Descendants("maxwind_kph").FirstOrDefault(),
                (string)npc.Descendants("avghumidity").FirstOrDefault(),
                (string)doc.Descendants("text").FirstOrDefault(),
            newBitMap
           });

            
            }
            dataGridView1.DataSource = dt;


        }
    }
        
}
